# Gift Giving (Reciprocal Altruism)
tags = #rapport

When someone does you a favor, you naturally want to reciprocate. You can take advantage of this in conversation. There are lots of different types of gifts you can give, such as compliments and material gifts.

Reference:
It's Not All About "Me"