# Ensure Tests Pass
tags = #code-review

When reviewing a change, ensure that tests still pass. It's possible to make a valid change for the actual production code while still breaking tests.