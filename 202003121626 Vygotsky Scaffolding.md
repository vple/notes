# Vygotsky Scaffolding
tags = #learning

Scaffolding is a technique to help others learn things that they don't know, but others do. The idea is that a "novice" and an "expert" work together, with the expert being able to guide the novice to accomplish things that they otherwise wouldn't be able to do independently. This helps the learner develop skills over time.

Resources:
- https://blog.prepscholar.com/vygotsky-scaffolding-zone-of-proximal-development