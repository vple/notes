# Sympathy or Assistance Theme
tags = #rapport

Making an easy and nonthreatening request is a tool to get others to take an action, give information, and/or talk with you. People are inclined to do good things for future reciprocation, and presenting a request with minimal cost gives people an easy way to do so.

Reference:
It's Not All About "Me"

