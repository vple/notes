# Accomodating Nonverbals
tags = #rapport #nonverbal-communication

You want to use nonverbals to help you look like a non-threatening, nice person to talk to.

These include:

- [[202003052219]] Smiling
- [[202003052227]] Head Tilt
- [[202003052233]] Lower Chin Angle
- [[202003052237]] Nonthreatening Body Angle
- [[202003052240]] Handshake

Reference:
It's Not All About "Me"