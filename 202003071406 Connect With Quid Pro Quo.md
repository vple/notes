# Connect With Quid Pro Quo
tags = #rapport

This techniques involves giving a bit of information about you to get information from others. The two main scenarios where you'd use it are when you're speaking with someone introverted and/or guarded, and where someone becomes suddenly awkward that they've been speaking too much. In the second case, you can help prevent the conversation from reaching that point by making sure to say a few quick sentences about every 15 minutes.

Reference:
It's Not All About "Me"

