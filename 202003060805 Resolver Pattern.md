# Resolver Pattern
tags = #design-patterns #dependency-injection

The resolver pattern is used to dynamically and declaratively determine an appropriate implementation for a particular situation. The resolver pattern has three parts:

1. A resolver object, responsible for determining the implementation.
2. A set of implementation options, each with their own properties.
3. An input specifying the constraints that the implementation must satisfy.

Crucial to this is that the implementation properties are defined on the interface, not on the implementations themselves. This allows the resolver to determine properties of the implementations, _without actually knowing what each implementation is_. The resolver can then essentially use a strategy pattern to determine an appropriate implementation based on the input.