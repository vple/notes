# Emotional Labeling
tags = #rapport

<!-- Referenced By: -->
[[202003071239]] Ask... How? When? Why?

As it sounds, emotional labeling is where you label the emotions that someone's having, particularly when they're displaying a lot of emotion.