# Establish Artificial Time Constraints
tags = #rapport

People are awkward or on guard when starting a conversation because they don't know when it will end. This is reduced by establishing an artificial time constraint, which you can do both nonverbally (indicating you have something else to do) or verbally.

Reference:
It's Not All About "Me"