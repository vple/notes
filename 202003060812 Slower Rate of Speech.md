# Slower Rate of Speech
tags = #rapport

Controlling how you speak can also make you more accommodating. Speaking more slowly and pausing helps others absorb what you've said. It also helps prevent coming across as if you're trying to sell something.

Reference:
It's Not All About "Me"