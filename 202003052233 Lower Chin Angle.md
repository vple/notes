# Lower Chin Angle
tags = #rapport #nonverbal-communication

<!-- Referenced By: -->
[[202003052217]] Accomodating Nonverbals

Keeping a slightly lower chin angle helps build rapport. Having a high chin angle makes it look like you're looking down your nose at someone, as if you're aloof or better than them.