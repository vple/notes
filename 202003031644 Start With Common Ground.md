# Start With Common Ground
tags = #effective-communication

When talking to someone, make sure to start from an agreed upon (implict or explicit) common ground. Often, a goal of communication is to lead someone through a certain way of thinking. It's not possible to do if their understanding of something is very different than yours—the idea in your head won't properly translate into theirs.