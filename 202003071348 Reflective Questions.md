# Reflective Questions
tags = #rapport

<!-- Referenced By: -->
[[202003071239]] Ask... How? When? Why?

Reflective questions involve repeating what the other person just said, as a question. This leads others to elaborate more on what they just said.

Reflective questions are useful because they help others expand on what they're saying, but you don't have to think of a question or something to say to get the person to speak more.

