# Nonthreatening Body Angle
tags = #rapport #nonverbal-communication

<!-- Referenced By: -->
[[202003052217]] Accomodating Nonverbals

Especially the first time you meet someone, you want to appear nonthreatining. Slightly angling your body away from the person you're talking to helps accomplish this.

