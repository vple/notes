# Ego Suspension
tags = #rapport

Suspending your ego tends to be difficult to do, but is a very good technique when you're trying to have a positive interaction and buid rapport. It can be especially difficult because you have to put on hold the believe that you're in the right for the sake of improving the relationship.

Reference:
It's Not All About "Me"

