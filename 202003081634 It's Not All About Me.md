# It's Not All About Me
tags = #rapport

1. [[202003052207]] Establish Artificial Time Constraints
2. [[202003052217]] Accomodating Nonverbals
3. [[202003060812]] Slower Rate of Speech
4. [[202003060817]] Sympathy or Assistance Theme
5. [[202003060832]] Ego Suspension
6. [[202003071230]] Validate Others
7. [[202003071239]] Ask... How? When? Why?
8. [[202003071406]] Connect With Quid Pro Quo
9. [[202003071830]] Gift Giving (Reciprocal Altruism)
10. [[202003081626]] Manage Expectations

