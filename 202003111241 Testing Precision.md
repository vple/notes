# Testing Precision
tags = #software-testing

When writing a test, you want to have precision. You want both your test to pass when submitting valid input, but you also want your test to fail when submitting invalid input. And, ideally, you have it work this way for all _classes of inputs_, not just one-off values engineered for the test.

This can often be challenging in practice. Ideally, the use of tools can help improve test precision while keeping tests easy to write.

Resources:

- https://kentcdodds.com/blog/make-your-test-fail