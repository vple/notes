# Changeable Values
tags = #modeling

When should a value be allowed to change, especially an identifier-like value?

Things that actually change often should be allowed to change.

For less clear things, you'll want to consider things such as:

- Technically, how hard is it to handle changing identifiers?
- Migration-wise, will it be chaotic for others to handle a changing id?
  - For example, if you have multiple servers relying on this id, how are you going to synchronize the change?

