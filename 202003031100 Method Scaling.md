# Method Scaling
tags = #method-design

When writing a method, it's important to think about how the method will behave _over time_, not just for the use case you have in mind at the moment. There's a few places to be aware of.

## Method Inputs
Especially with collections as inputs—how will the inputs to this method vary over time? How large of an input can it handle?

## Data
How much data is getting processed in this method? How does this scale?

## I/O
When performing input/output, how much input/output are you expecting? If you're fetching all objects by some passed in ids, this means you could end up fetching a very large amount of objects from, e.g., a database. Is that feasible?