# Tracking Project Work
tags = #project-management

Effective project management requires keeping track of work. There's lots of different types of work, and we need ways to track each scenario.

- Future work, which may not be implemented any time soon. May be vague.
- Future work, to be implemented soon.
- Work in progress.
- Completed work.
- Work that isn't going to be worked on.