# Conversation Threading
tags = #communication

<!-- Referenced By: -->
[[202003071239]] Ask... How? When? Why?

Threading a conversation is where you introduce a new line of conversation, based on an existing line of conversation. Essentially, you're branching off. This gives you the potential for more things to talk about.

When trying to engage more along that line of conversation, you'll want to pay attention to how far and deep the other person is willing to go. If they don't engage much, you'll want to drop that line.