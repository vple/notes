# Manage Expectations
tags = #rapport

People always have an agenda in a conversation. Those that can mask their agenda or shift the agenda to something positive will have better success at building rapport.

Reference:
It's Not All About "Me"