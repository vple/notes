# Minimal Encouragers
tags = #rapport

<!-- Referenced By: -->
[[202003071239]] Ask... How? When? Why?

A minimal encourager is a small action that shows you're paying attention and listening. These include head nods; saying "uh huh," "yes," "I understand"; etc.

Using minimal encouragers insincerely or too much can cause others to view you as uncaring or cold.

