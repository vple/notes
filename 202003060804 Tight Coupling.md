# Tight Coupling
tags = #dependency-injection

Tight coupling is where one object has a strong or "named" dependency on another. Dependency injection is about breaking these tight couplings.

